const passportJWT = require('../lib/passportjwt');

module.exports = passportJWT.authenticate('jwt', { session: false });
