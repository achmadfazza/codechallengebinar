var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// tambahkan express sessin dan flash
var session = require('express-session');
var flash = require('express-flash');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiAuthRouter = require('./routes/api/auth');

var app = express();
// menambahkan library pasport
const passport = require('./lib/passport');
const passportJWT = require('./lib/passportjwt');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
// setting view engine dan ejs
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
// setting body parser
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// setting session handler
app.use(
  session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false,
  })
);
app.use(flash());
// jangan lupa nambahin ini karena ini fungsi
app.use(passport.initialize());
app.use(passport.session());
app.use(passportJWT.initialize());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/auth', apiAuthRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
