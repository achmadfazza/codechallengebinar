'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Room.player1_id = models.Room.belongsTo(models.User, {
        foreignKey: 'player1_id',
      });
      models.Room.player2_id = models.Room.belongsTo(models.User, {
        foreignKey: 'player2_id',
      });
    }
  }
  Room.init(
    {
      room_name: DataTypes.STRING,
      player1_id: DataTypes.INTEGER,
      player2_id: DataTypes.INTEGER,
      player1_hand: DataTypes.STRING,
      player2_hand: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Room',
    }
  );
  return Room;
};
