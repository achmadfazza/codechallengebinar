const { Model } = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}
    // method melakukan enkripsi
    static #encrypt = function (password) {
      return bcrypt.hashSync(password, 10);
    };
    // method register
    static register = function ({ username, password }) {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ username, password: encryptedPassword });
    };
    // fungsi cek untuk compare pass dengan table
    checkpassword = (password) => {
      return bcrypt.compareSync(password, this.password);
    };
    //fungsi json web
    generateToken = () => {
      const Payload = {
        id: this.id,
        username: this.username,
      };
      const Secret = 'ini rahasia';
      return jwt.sign(Payload, Secret);
    };

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) {
          return Promise.reject('invalid username and password');
        }
        if (!user.checkpassword(password)) {
          return Promise.reject('invalid username and password');
        }
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }

  User.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'User',
    }
  );
  return User;
};
