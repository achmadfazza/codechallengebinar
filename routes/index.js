var express = require('express');
var router = express.Router();
// import controller
const authController = require('../controllers/authcontroller');
const restrict = require('../middleware/restrict');

/* GET home page. */
router.get('/', restrict, function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/register', (req, res) => res.render('register'));
router.post('/register', authController.registerAction);
router.get('/login', (req, res) => res.render('login'));
router.post('/login', authController.loginaction);
router.get('/session', restrict, authController.usersessionaction);

module.exports = router;
