var express = require('express');
var router = express.Router();
// import controller
const authController = require('../../controllers/api/authcontroller');

const restrict = require('../../middleware/restrictjwt');
router.post('/register', authController.registerAction);
router.post('/login', authController.loginaction);
router.get('/profile', authController.profileAction);
router.post('/creatematch', authController.createMatchAction);

router.post('/fight/:room_id', restrict, authController.fightaction);

module.exports = router;
