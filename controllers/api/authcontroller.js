const { User, Room } = require('../../models');

const registerAction = function (req, res, next) {
  User.register(req.body)
    .then((user) =>
      res.json({
        id: user.id,
        username: user.username,
      })
    )
    .catch((err) => next(err));
};

const loginaction = function (req, res, next) {
  User.authenticate(req.body)
    .then((user) => {
      res.json({ id: user.id, username: user.username, token: user.generateToken() });
    })
    .catch((err) => next(err));
};

const profileAction = (req, res, next) => {
  res.json(req.user);
};

const createMatchAction = function (req, res, next) {
  Room.create({
    room_name: req.body.room_name,
  }).then((room) =>
    res.json({
      id: room.id,
      room_name: room.room_name,
    })
  );
};

const fightaction = function (req, res, next) {
  console.log(req.user);
};
module.exports = { registerAction, loginaction, profileAction, createMatchAction, fightaction };
