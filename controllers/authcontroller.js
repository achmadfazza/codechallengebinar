// handler auth
// import passport
const { User } = require('../models');
const passport = require('passport');

const registerAction = function (req, res, next) {
  User.register(req.body)
    .then(() => res.redirect('/login'))
    .catch((err) => next(err));
};
const loginaction = passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true,
});

const usersessionaction = (req, res, next) => {
  res.render('session', { username: req.user.username });
};

module.exports = { registerAction, loginaction, usersessionaction };
